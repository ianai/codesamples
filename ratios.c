//
//  main.c
//  ratios
//
//  Created by Ian Schroeder-Anderson on 9/20/15.
//  Copyright © 2015 Ian Schroeder-Anderson. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int FindRatio(double val, double tolerance, int *numeratorptr, int *denominatorptr, long max_iterations) {
    int status = 1;
    if( ! numeratorptr || ! denominatorptr ) {
        return(status);
    }
    int num = 1, denom = 1;
    double currentVal = (double) num/ (double) denom;
    double lowBound = val - tolerance;
    double upperBound = val + tolerance;
    
    for (int i = 0; i < max_iterations; i++) {
        currentVal = (double) num/ (double) denom;
        if ( (lowBound <= currentVal) && (currentVal <= upperBound) ) {
            status = 0;
            break;
        } else {
            if (upperBound < currentVal) {
                denom++;
            } else {
                num++;
            }
        }
    }
    
    *numeratorptr = num;
    *denominatorptr = denom;
    
    return(status);
}

int main(int argc, const char * argv[]) {
    /*Implement a function to return a ratio from a double (ie 0.25 -> 1/4). The function will also take a tolerance so if toleran ce is .01 then FindRatio(.24, .01) -> 1/4
     int FindRatio(double val, double tolerance, int& numerator, int& denominator)
     */
    double val = 0.24, tolerance = 0.01;
    int * numptr = (int *) malloc(sizeof(int)) ;
    int * denptr = (int * ) malloc(sizeof(int));
    FindRatio(val, tolerance, numptr, denptr, 10000);
    
    printf("FindRatio(%f, %f) -> %i\\%i\n", val, tolerance, *numptr, *denptr);
    val = .32;
    FindRatio(val, tolerance, numptr, denptr, 10000);
    printf("FindRatio(%f, %f) -> %i\\%i\n", val, tolerance, *numptr, *denptr);
    
    val = .83;
    FindRatio(val, tolerance, numptr, denptr, 10000);
    printf("FindRatio(%f, %f) -> %i\\%i\n", val, tolerance, *numptr, *denptr);
    
    val = .935;
    FindRatio(val, tolerance, numptr, denptr, 10000);
    printf("FindRatio(%f, %f) -> %i\\%i\n", val, tolerance, *numptr, *denptr);
    
    val = .3824;
    tolerance = 0.0001;
    FindRatio(val, tolerance, numptr, denptr, 10000);
    printf("FindRatio(%f, %f) -> %i\\%i\n", val, tolerance, *numptr, *denptr);
    
    val = .315621;
    tolerance = 0.00001;
    FindRatio(val, tolerance, numptr, denptr, 10000);
    printf("FindRatio(%f, %f) -> %i\\%i\n", val, tolerance, *numptr, *denptr);

    val = .71562108;
    tolerance = 0.0000001;
    FindRatio(val, tolerance, numptr, denptr, 10000);
    printf("FindRatio(%f, %f) -> %i\\%i\n", val, tolerance, *numptr, *denptr);
    free(numptr);
    free(denptr);
    
    /*
     Output from above:
     FindRatio(0.240000, 0.010000) -> 1\4
     FindRatio(0.320000, 0.010000) -> 5\16
     FindRatio(0.830000, 0.010000) -> 5\6
     FindRatio(0.935000, 0.010000) -> 13\14
     FindRatio(0.382400, 0.000100) -> 13\34
     FindRatio(0.315621, 0.000010) -> 95\301
     FindRatio(0.715621, 0.000000) -> 2909\4065
     */
    return 0;
}
