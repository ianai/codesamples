//
//  main.c
//  linkedlists
//
//  Created by Ian Schroeder-Anderson on 9/7/15.
//  Copyright (c) 2015 Ian Schroeder-Anderson. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct _doubleNode {
    struct _doubleNode * next;
    struct _doubleNode *prev;
    int value;
    bool mark;
    
} doubleNode ;

typedef struct _doubleLinkedList {
    doubleNode *head;
    doubleNode *tail;
    doubleNode *current;
    
} doubleLinkedList ;

void printList(doubleLinkedList* list) {
    if( list!= NULL && list->head != NULL) {
        doubleNode *node = list->head;
        while (node != NULL) {
            printf("node value is %i\n", node->value );
            node = node->next;
        }
    }
}

void initializeList(doubleLinkedList * doubleLL) {
    doubleLL->head = NULL;
    doubleLL->tail = NULL;
    doubleLL->current = NULL;
}

doubleNode *createNodeOnHeap(doubleNode *next, doubleNode * prev, int value) {
    doubleNode *newNode = (doubleNode *) malloc(sizeof(doubleNode)) ;
    if( newNode != NULL) {
        newNode->next = next;
        newNode->prev = prev;
        newNode->value = value;
        newNode->mark = false;
    }
    return(newNode);
}

void addNodeAtHead(doubleLinkedList *list, doubleNode * node) {
    if(list != NULL && node != NULL) {
        if(list->head == NULL) {
            list->head = node;
            
        } else {
            node->next = list->head;
            node->prev = NULL;
            list->head->prev = node;
            list->head = node;
        }
    }
    if (! list-> tail ) {
        list->tail = node;
    }
}

void addNodeAtTail(doubleLinkedList *list, doubleNode* node) {
    if(list != NULL && node != NULL) {
        if(list->head == NULL) {
            list->head = node;
        }
        
        if(list->tail == NULL) {
            list->tail = node;
        } else {
            node->prev = list->tail;
            node->next = NULL;
            list->tail->next = node;
            list->tail = node;
        }
    }
}

void addValueAtTail(doubleLinkedList *list, int value) {
    doubleNode * node = createNodeOnHeap(NULL, NULL, value);
    addNodeAtTail(list, node);
}
void addValueAtHead(doubleLinkedList *list, int value) {
    doubleNode * newNode = createNodeOnHeap(NULL, NULL, value);
    addNodeAtHead(list, newNode);
}

void markAllNodes(doubleLinkedList *list, bool value) {
    list->current = list->head;
    while(list->current != list->tail) {
        list->current->mark = value;
        list->current = list->current->next;
    }
    return;
}

void addCycleToListAtPoint(doubleLinkedList *list, int point) {
    if(list->head == NULL || list->tail == NULL) { return;};
    
    doubleNode* node = list->head;
    doubleNode *cycleNode=NULL;
    int i = 0;
    while (node->next != list->tail && i <= point) {
        if( i == point ) {
            cycleNode = node;
        }
        node = node->next;
        i++;
    }
    if(cycleNode != NULL) {
        list->tail->next = cycleNode;
    }
    
    return;
}

bool isListACycle(doubleLinkedList *list, int max_iterations) {
    doubleNode * slow = list->head;
    doubleNode* fast = list->head->next->next;
    int i = 0;
    
    while(i <= max_iterations && slow != NULL && slow->next != NULL && fast != NULL && fast->next != NULL && fast->next->next!= NULL) {
        i++;
        if (slow == fast || i == max_iterations) {
            if (i== max_iterations) {
                printf("Cycle test met max_iterations before exiting.");
            }
            return true;
        }
        slow = slow->next;
        fast = fast->next->next;
    }
    return false;
}

int findSizeSimple(doubleLinkedList *list) {
    list->current = list->head;
    int i = 0;
    while(list->current != list->tail) {
        i++;
        list->current = list->current->next;
    }
    i++; // for the tail
    return(i);
}

int findSizeUsingMarks(doubleLinkedList *list) {
    list->current = list->head;
    markAllNodes(list, false);
    int i=0;
    doubleNode *currentNode = list->head;
    while(currentNode != NULL) {
        if(currentNode->mark) {
            break;
        } else {
            i++;
            currentNode->mark = true;
            currentNode = currentNode->next;
        }
    }
    return(i);
}

int findSizeNoTail(doubleLinkedList *list) {
    doubleNode *slow = list->head;
    doubleNode *fast = list->head->next->next;
    int i=0;
    while( slow != NULL && slow->next != NULL && fast != NULL && fast->next != NULL && fast->next->next != NULL ) {
        
        if (slow == fast) {
            break;
        }
        slow = slow->next;
        fast = fast->next->next;
        i=i+2;
    }
    return(i);
}

int findSizeTwoPaths(doubleLinkedList *list) {
    doubleNode *forward = list->head;
    doubleNode *backward = list->tail;
    
    int forwardCount, backwardCount;
    forwardCount = backwardCount = 0;
    while (forward != NULL && backward != NULL) {
        if(forward == backward || forward->next == backward ) {
            return(forwardCount + backwardCount);
        } else {
            forwardCount++;
            backwardCount++;
            forward = forward->next;
            backward = backward->prev;
            
        }
    }
    return(forwardCount + backwardCount);
}

doubleNode* findMiddleNoMarks(doubleLinkedList *list) {
    //this assumes list contains no cycles.
    doubleNode *slow = list->head;
    doubleNode *fast = list->head->next->next;
    
    while(fast && fast->next && fast->next->next) {
        slow = slow->next;
        fast = fast->next->next;
    }
    return(slow);
}

void pivot(doubleLinkedList * list, int value, doubleLinkedList * gt, doubleLinkedList * lt) {
    list->current = list->head;
    
    while(list->current != list->tail) {
        if( list->current->value < value ) {
            addValueAtHead(lt, list->current->value);
        } else if(list->current->value > value) {
            addValueAtHead(gt, list->current->value);
        }
        list->current = list->current->next;
    }
}

doubleLinkedList * createCountingList(int endValue) {
    doubleLinkedList* list = (doubleLinkedList*)malloc((sizeof(doubleLinkedList)));
    initializeList(list);
    
    for(int i = 0; i<= endValue; i++){
        addValueAtHead(list, i);
        
    }
    
    return(list);
    
}
void freeNode(doubleNode* node) {
    //helpful if/when node's grow in complexity to require more than one line to delete
    free(node);
}

void freeAllNodesAndLinkedList(doubleLinkedList *linkedList) {
    if( linkedList->head == NULL && linkedList->tail == NULL) {
        free(linkedList);
        return;
    }
    doubleNode *next = NULL;
    if(linkedList->head != NULL && linkedList->tail != NULL) {
        linkedList->current = linkedList->head;
        while(linkedList->current != linkedList->tail) {
            next = linkedList->current->next;
            freeNode(linkedList->current);
            linkedList->current = next;
        }
        
    }
    
    free(linkedList);
    return;
    
}

int main(int argc, const char * argv[]) {
    /*todo:
     x-Make a linked list
     x-Store some values to the linked list
     x-Print the linked list
     x-Detect a cycle in the linked list
     x--make sure it works for a cycled list and an uncycled list
     x-Create a linked list with a cycle
     x-Create a sorted linked list
     x-Figure out how long a list is despite it having a cycle
     x-Pivot the linked list at a pivot point
     x-memory management
     x-Find the middle of a linked list. Now do it while only going through the list once. (same solution as finding cycles)
     */
    
    doubleLinkedList * list=(doubleLinkedList*) malloc(sizeof(doubleLinkedList));
    initializeList(list);
    for (int i=0; i<=30; i++ ) {
        addValueAtHead(list, i);
    }
    
    addValueAtTail(list, 62);
    
    printList(list);
    if( isListACycle(list, 40) ) {
        printf("The first cycle test failed.\n");
    } else {
        printf("The first cycle test passed.\n");
    }
    
    addCycleToListAtPoint(list, 7);
    
    if( isListACycle(list, 40) ) {
        printf("The second cycle test passed.\n");
    } else {
        printf("The second cycle test failed.\n");
    }
    
    doubleLinkedList * list2 = createCountingList(5);
    printf("Second linked list of size 6 counting 'to' 5:\n");
    printList(list2);
    
    printf("Calculate size of list, simple method: %i\n", findSizeSimple(list) );
    printf("Calculate size of list, marking method: %i\n", findSizeUsingMarks(list) );
    printf("Calculate size of list, no tail method: %i\n", findSizeNoTail(list) );
    printf("Calculate size of list, two paths method: %i\n", findSizeTwoPaths(list) );
    
    
    freeAllNodesAndLinkedList(list);
    freeAllNodesAndLinkedList(list2);
    
    doubleLinkedList *mainList = createCountingList(20);
    doubleLinkedList *lt = (doubleLinkedList*) malloc(sizeof(doubleLinkedList));
    doubleLinkedList *gt = (doubleLinkedList*) malloc(sizeof(doubleLinkedList));
    initializeList(lt);
    initializeList(gt);
    int pivotVal = 10;
    pivot(mainList, pivotVal, gt, lt);
    printf("This list should all be under the value( %i ) \n", pivotVal);
    printList(lt);
    printf("\nThis list should all be over the value ( %i )\n", pivotVal);
    printList(gt);
    
    doubleNode * midTest = findMiddleNoMarks(mainList);
    
    printf("\nThe mainList:\n");
    printList(mainList);
    if (midTest) {
        printf("\nThe middle of mainList was found to be at value %i", midTest->value);
    } else {
        printf("\nThe middle test returned null.");
    }
    
    
    freeAllNodesAndLinkedList(mainList);
    freeAllNodesAndLinkedList(lt);
    freeAllNodesAndLinkedList(gt);
    
    return 0;
}
