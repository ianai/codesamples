//
//  main.c
//  lightbulb_puzzle
//
//  Created by Ian Schroeder-Anderson on 9/20/15.
//  Copyright © 2015 Ian Schroeder-Anderson. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

typedef struct _bulb {
    int fail_floor;
} test_bulb ;

bool surviveTest(test_bulb bulb, int tryFloor, int * tries_used, int * bulbs_broken) {
    //true means it survived
    (*tries_used)++;
    if( bulb.fail_floor <= tryFloor) {
        (*bulbs_broken)++;
        return(false);
    } else {
        return(true);
    }
}

int floorOfFailure( test_bulb bulb, int max_tries, int * tries_used , int *bulbs_broken ) {
    int fail_floor = -1;
    int max_floor = 100;
    int little_step = 1;
    int big_step = 10;
    int currentStep = big_step;
    bool currentTry = false;
    
    for (int currentFloor = 0; currentFloor <= max_floor ; currentFloor = currentFloor + currentStep) {
        currentTry =  surviveTest(bulb, currentFloor, tries_used, bulbs_broken);
        if (currentStep == little_step && ! currentTry ) {
            fail_floor = currentFloor;
            break;
        } else if ( !currentTry ) {
            currentStep = little_step;
            currentFloor = currentFloor - big_step;
        }
    }
  return(fail_floor);
}

int main(int argc, const char * argv[]) {
    /*You have 2 supposedly unbreakable light bulbs and a 100-floor building. Using fewest possible drops, determine how much of an impact this type of light bulb can withstand. (i.e. it can withstand a drop from 17th floor, but breaks from the 18th).
     Note that the ever-popular binary search will give you a worst case of 50 drops. You should be able to do it with under 20.
     */
    
    test_bulb bulb;
    bulb.fail_floor = 99;
    
    int * tries_used = (int*)malloc(sizeof(int));
    *tries_used = 0;
    int * bulbs_broken = (int*)malloc(sizeof(int));
    *bulbs_broken = 0;
    int failfloor = floorOfFailure(bulb, 10000, tries_used, bulbs_broken);
    
    printf("For 100 stories the algorithm used %i bulbs after %i tries. The fail floor was %i\n", *bulbs_broken, *tries_used, failfloor);
    
    free(tries_used);
    free(bulbs_broken);
    
    
    return 0;
}
