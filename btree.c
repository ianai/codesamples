//
//  main.c
//  btree
//
//  Created by Ian Schroeder-Anderson on 10/27/15.
//  Copyright © 2015 Ian Schroeder-Anderson. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

typedef struct btree_node {
    struct btree_node * parent;
    struct btree_node * left;
    struct btree_node * right;
    void * data;
} btree_node ;

void init_btree_node(btree_node * node, btree_node * parent, btree_node * left, btree_node * right) {
    node->parent = parent;
    node->left = left;
    node->right = right;
}

int compareInts(btree_node * nodeA, btree_node * nodeB) {
    //assuming the data for the nodes are stored as ints
    int value = *(int*)(nodeA->data) - *(int*)(nodeB->data) ;
    if (value == 0) {
        return(0);
    } else if (value < 0 ) {
        return(-1);
    } else if (value > 0) {
        return(1);
    }
    
    return(value);
}

void insert(btree_node * root, btree_node * newNode, int (*fptr_COMPARE_FN)(btree_node * , btree_node * ) )  {
    btree_node * currentNode = root;
    int compare;
    while(currentNode) {
        compare = (*fptr_COMPARE_FN)(currentNode, newNode);
        if(compare == 0) {
            printf("In binary tree insertion attempted to insert a duplicate value, ignoring\n" );
            break;
        }
        if(compare < 0 ) {
            if(!currentNode->left) {
                currentNode->left = newNode;
                return;
            } else {
                currentNode = currentNode->left;
            }
        } else if (compare > 0) {
            if(! (currentNode->right ) ) {
                currentNode->right = newNode;
                return;
            } else {
                currentNode = currentNode->right;
            }
        }
    }
    
    printf("Unable to input newNode with data %i into binary tree\n", *(int*)(newNode->data) );
    return;
}

void PrintInOrder(btree_node * root) {
    //we'll assume the data are ints here

    if(root->left) {
        PrintInOrder(root->left);
    }
    
    if(root->data) {
        printf("The value for this node is %i\n", *(int*) root->data);
    }
    
    if(root->right) {
        PrintInOrder(root->right);
    }
    
}

void PrintPostOrder(btree_node * root) {
    if(root->left) {
        PrintPostOrder(root->left);
    }
    
    if(root->right) {
        PrintPostOrder(root->right);
    }
    
    if(root->data) {
        printf("The value for this node is %i\n", *(int*) root->data);
    }
}

void nr_PrintInOrder(btree_node *root) {
    
}

void freeAll(btree_node * root) {
    if(root->right) {
        freeAll(root->right);
    }
    if(root->left) {
        freeAll(root->left);
    }
    free(root->data);
    free(root);
}

void nr_freeAll(btree_node * root) {
    
    btree_node * currentNodeLeft = root->left, *currentNodeRight = root->right;
    btree_node *currentNode = root, *nextCurrentNode = NULL, *previousNode = root;
    
    while( ( (root->left) || (root->right) ) || root != nextCurrentNode ) {
        currentNodeLeft = currentNode->left;
        currentNodeRight = currentNode->right;
        
        if(currentNodeLeft) {
            nextCurrentNode = currentNode;
            previousNode = currentNode;
            currentNode = currentNodeLeft;
            continue;
        }
        
        if(currentNodeRight) {
            nextCurrentNode = currentNode;
            previousNode = currentNode;
            currentNode = currentNodeRight;
            continue;
        }
        if ( (!currentNodeLeft ) && (!currentNodeRight) ){
            if (currentNode->data) {
                printf("freeing node with data %i\n", *(int*) currentNode->data );
                free(currentNode->data);
            };
            if(previousNode->left == currentNode) {nextCurrentNode->left = NULL; }
            if(previousNode->right == currentNode) {nextCurrentNode->right = NULL;}
            
            free(currentNode);
            currentNode = root;
            nextCurrentNode = root;
            previousNode = root;
            continue;
        }
    }
    if (root->data) {free(root->data);}
    if (root) {free(root);}
}

int main(int argc, const char * argv[]) {
    /*Binary Trees
     
     - Implement the following functions for a binary tree:
     
     - Insert
     - PrintInOrder
     - PrintPreOrder
     - PrintPostOrder
     - Implement a non-recursive PrintInOrder
     */
    printf("begin\n");
    btree_node *root, * newNode;
    root = (btree_node*)malloc(sizeof(btree_node));
    root->data = (int * ) malloc(sizeof(int));
    root->data = NULL;
    root->left = NULL;
    root->right = NULL;

    int * initData = (int*) malloc(sizeof(int));
    *initData = 19;
    
    root->data = initData;
    
    //load up some test data

    for (int i = 0; i<=20; i++) {
        initData = (int * ) malloc(sizeof(int));
        *initData = 3*i - ( pow(-1.0, i)*2*i );
        newNode = (btree_node * ) malloc(sizeof(btree_node));
        newNode->left = NULL;
        newNode->right = NULL;
        newNode->data = (void*) initData;
        insert(root, newNode, &compareInts );
    }
    printf("Printing in order:\n");
    PrintInOrder(root);
    printf("\n\nPrinting in post order:\n");
    PrintPostOrder(root);
    
    printf("Freeing all the nodes - no recursion\n");
    nr_freeAll(root);
    
    return 0;
}
