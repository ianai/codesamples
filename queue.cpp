//
//  main.cpp
//  queue
//
//  Created by Ian Schroeder-Anderson on 10/11/15.
//  Copyright © 2015 Ian Schroeder-Anderson. All rights reserved.
//

#include <iostream>

typedef struct _node {
    _node * next;
    _node * prev;
    void * data;
} Node ;

typedef struct _linkedList {
    Node * head;
    Node * tail;
    Node * currentNode ;
} LinkedList ;

void initializeLinkedList(LinkedList * list, Node * head, Node * tail) {
    list->head = head;
    list->tail = tail;
    list->currentNode = nullptr;
}

void initalizeList(LinkedList *list) {
    initializeLinkedList(list, nullptr, nullptr);
}

Node * createNodeOnHeap(Node * next, Node * prev, void * data) {
    Node * newNode = (Node * ) malloc(sizeof(Node));
    newNode->next = next;
    newNode->prev = prev;
    newNode->data = data;
    
    return(newNode);
}

void checkTail(LinkedList * list) {
    if( list->head && ! list->tail) {
        //it's clearly not empty, but the tail hasn't been set.
        list->currentNode = list->head;
        Node * newTail = list->head;
        while(list->currentNode != nullptr) {
            newTail = list->currentNode;
            list->currentNode = list->currentNode->next;
        }
        list->tail = newTail;
    }
}

void checkHead(LinkedList * list) {
    if(list->tail && !list->head ) {
        list->currentNode = list->tail;
        Node * newHead = list->tail;
        while(list->currentNode != nullptr) {
            newHead = list->currentNode;
            list->currentNode = list->currentNode->prev;
        }
        list->head = newHead;
    }
        
}

void sanitizeList(LinkedList * list) {
    checkHead(list);
    checkTail(list);
}

void addHead(LinkedList * list, void * data) {
    Node * oldHead;
    Node * newHead;
    if( list->head ) {
        oldHead = list->head;
        newHead = createNodeOnHeap(oldHead, nullptr, data);
        oldHead->prev = newHead;
    } else {
        newHead = createNodeOnHeap(nullptr, nullptr, data);
    }
    
    list->head = newHead;
    sanitizeList(list);
}

void addTail(LinkedList * list, void * data) {
    Node * oldTail = list->tail;
    Node * newTail = createNodeOnHeap(nullptr, oldTail, data);
    if(oldTail) {
        oldTail->next = newTail;
    }

    list->tail = newTail;
    sanitizeList(list);
}

void printList(LinkedList* list) {
    list->currentNode = list->head;
    while(list->currentNode) {
        std::cout << "Node value is %f.\n" << list->currentNode->data;
        list->currentNode = list->currentNode->next;
    }
}

void deleteNode(Node *node) {
    free(node->data);
    free(node);
}

void deleteLinkedList(LinkedList * list){
    if( list->head && list->tail) {
        Node * next;
        list->currentNode = list->head;
        while(list->currentNode != list->tail) {
            next = list->currentNode->next;
            deleteNode(list->currentNode);
            list->currentNode = next;
        }
        deleteNode(list->tail);
        free(list);
    }
}

typedef LinkedList Queue;

void initializeQueue(Queue * queue) {
    initalizeList(queue);
}

void enqueue(Queue *queue, void * data) {
    addHead(queue, data);
}

void * dequeueFIFO(Queue* queue) {
    if (queue->tail) {
        Node * node = queue->tail;
        queue->tail = node->prev;
        void * data = node->data;
        free(node);
        return(data);
    } else {
        return(nullptr);
    }
}

void deleteQueue(Queue * queue) {
    deleteLinkedList(queue);
}

int main(int argc, const char * argv[]) {
    /*
     The purpose of this is to demonstrate a queue in C++ backed by a linked list.
     
     My solution here is using C structs instead of C++ classes for the nodes and linked list. 
     A more C++-esque version could use C++ classes for the objects.
     */
    
    Queue * queue = (Queue *) malloc(sizeof(Queue));
    initializeQueue(queue);

    
    for (int i = 0; i <= 40; i++) {
        enqueue(queue, (new int(i) ));
    }
    int * i;
    
    while( ( i = (int*) dequeueFIFO(queue) ) != nullptr ) {
        std::cout << "Dequeued value is " << *i << "\n";
    }
    
    deleteLinkedList(queue);
    
    return 0;
}