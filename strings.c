//
//  main.c
//  strings
//
//  Created by Ian Schroeder-Anderson on 9/13/15.
//  Copyright (c) 2015 Ian Schroeder-Anderson. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

char * reverseWords(char* inputs) {
    //this version returns memory that must be dealloc'd
    long inputLen = strlen(inputs);
    char * outputs = (char*) malloc(inputLen);
    for (long i = inputLen; i>= 0; i--){
        outputs[i] = inputs[inputLen-i-1];
    }
    
    return(outputs);
}

char * stripWhiteSpace(char * inputs) {
    long inputLen = strlen(inputs);
    char * outputs = (char*) malloc(inputLen);
    long whiteCount = 0;
    for (long i = 0; i <= inputLen ; i++) {
        if (!isspace(*&inputs[i])) {
            outputs[i-whiteCount] = inputs[i];
        } else {
            whiteCount++;
        }
    }
    return(outputs);
}

char * uniqueCharacters(char * inputs) {
    long inputLen = strlen(inputs);
    char * outputs = (char*) malloc(inputLen);
    for(int i = 0; i< inputLen ; i++) {
        if (! strchr(outputs, *&inputs[i]) ){
            strncat(outputs, &inputs[i], 1);
        }
    }
    strcat(outputs, "\0");
    realloc(outputs, strlen(outputs));
    
    return(outputs);
}

char  firstNonRepeat(char * inputs) {
    char * uniques = uniqueCharacters(inputs);
    char firstDupe = 0;
    
    char * first = NULL, * last = NULL;
    long inputLen = strlen(uniques);
    for(long i = 0; i < inputLen; i++ ) {
        /*
        find the first and last instances of each character
         if they're the same then it's unique
         The order of the unique string assures it's the first to be not repeated.
         
         */
        first = strchr(inputs, *&uniques[i]);
        last = strrchr(inputs, *&uniques[i]);
        if (first && last && first == last) {
            return(*&uniques[i]);
        }
       
    }
    
    free(uniques);
    return(firstDupe);
}

int main(int argc, const char * argv[]) {
    /*Strings
     
     ?x- Reverse words in a string (words are separated by one or more spaces). Now do it in-place. By far the most popular string question!
     x- Reverse a string
     x- Strip whitespace from a string in-place
     x-void StripWhitespace(char* szStr)
     x- Remove duplicate chars from a string ("AAA BBB" -> "A B")
     int RemoveDups(char* szStr)
     x- Find the first non-repeating character in a string:("ABCA" -> B )
     int FindFirstUnique(char* szStr)
     
     -in place version of reverse words
     
     - More Advanced Topics:
     - You may be asked about using Unicode strings. What the interviewer is usually looking for is:
     - each character will be two bytes (so, for example, char lookup table you may have allocated needs to be expanded from 256 to 256 * 256 = 65536 elements)
     - that you would need to use wide char types (wchar_t instead of char)
     - that you would need to use wide string functions (like wprintf instead of printf)
     - Guarding against being passed invalid string pointers or non nul-terminated strings (using walking through a string and catching memory exceptions
     */
    
    char * inputString = NULL;
    inputString = (char*) malloc(strlen("AAcA BBB"));
    strcpy(inputString, "AAcA BBB");
    
    char * reversedStringCopied = reverseWords(inputString);
    printf("The reversed version follows: \n%s\n", reversedStringCopied);
    char * strippedString = stripWhiteSpace(inputString);
    printf("The stripped version:\n%s\n", strippedString);
    
    char * uniques = uniqueCharacters(inputString);
    printf("The uniques of the string:\n%s\n", uniques);
    printf("The first non repeating in test string was '%c'.\n", firstNonRepeat(inputString) );
    
    free(uniques);
    free(inputString);
    free(reversedStringCopied);
    free(strippedString);
    return 0;
}
